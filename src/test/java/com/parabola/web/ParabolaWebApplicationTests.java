package com.parabola.web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;

import com.parabola.ParabolaWebApplication;

import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ParabolaWebApplication.class)
@WebAppConfiguration
public class ParabolaWebApplicationTests {

	@Test
	public void contextLoads() {
	}

}

package com.parabola;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParabolaWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParabolaWebApplication.class, args);
	}
}

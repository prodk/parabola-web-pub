package com.parabola;

import java.util.Map;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.parabola.data.CurrentUser;
import com.parabola.data.HyperbolaInputData;
import com.parabola.data.InputData;
import com.parabola.data.ParabolaInputData;
import com.parabola.data.ProductType;
import com.parabola.services.UserService;

@Controller
@PreAuthorize("@checkCurrentUserServiceImpl.canAccessUser(principal, #userId)")
public class UserController {
	private final UserService userService;
	
	private Logger log = Logger.getLogger(UserController.class);
	
	@Autowired
	public UserController(UserService service) {
		this.userService = service;
	}

	@RequestMapping(value="/user/{userId}", method=RequestMethod.GET)
	public String listUserProducts(@PathVariable Long userId, Model model) {
		model.addAttribute("products", userService.listUserProducts(userId));
		model.addAttribute("history", userService.listUserExpiredProducts(userId));
		
		final Boolean hasParabola = userService.hasProduct(userId, ProductType.parabola.toString());
		log.info("hasParabola " + hasParabola.toString());
		model.addAttribute("hasParabola", hasParabola);
		
		final Boolean hasHyperbola = userService.hasProduct(userId, ProductType.hyperbola.toString());
		log.info("hasHyperbola " + hasHyperbola.toString());
		model.addAttribute("hasHyperbola", hasHyperbola.toString());
		
		final Boolean hasActiveParabola = userService.hasActiveProduct(userId, ProductType.parabola.toString());
        log.info("hasActiveParabola " + hasActiveParabola.toString());
        model.addAttribute("hasActiveParabola", hasActiveParabola);
        
        final Boolean hasActiveHyperbola = userService.hasActiveProduct(userId, ProductType.hyperbola.toString());
        log.info("hasActiveHyperbola " + hasActiveHyperbola.toString());
        model.addAttribute("hasActiveHyperbola", hasActiveHyperbola);

		return "productsList";
	}
	
	// TODO: think how to allow the admin to use this link
	@PreAuthorize("hasAuthority('USER')")
	@RequestMapping(value="/user/try/{productType}", method=RequestMethod.GET)
	public String tryProduct(@PathVariable String productType, Model model, Authentication auth) {	    
	    CurrentUser currentUser = (CurrentUser) auth.getPrincipal();
	    
	    if (userService.createTrialProduct(currentUser.getId(), productType)) {
	        log.info("Free trial, product type:" + productType + ", user " +currentUser.getUsername());
	    }
	    else {
	        log.info("Failed to create free trial, product type:" + productType + ", user " +currentUser.getUsername());
	    }

	    // TODO: redirect to a free trial page where the message of activation and terms of usage are listed
	    // TODO: provide 2 links: go to the product or back to the list of products
	    return "redirect:/";
    }
	
	// TODO: reconsider this architecture: probably remove this or the next method
	@PreAuthorize("hasAuthority('USER')")
    @RequestMapping(value="/user/purchase/{productType}", method=RequestMethod.GET)
    public String purchaseProduct(@PathVariable String productType, Model model, Authentication auth) {
	    String result = "/";
	    CurrentUser currentUser = (CurrentUser) auth.getPrincipal();
	    
	    if (userService.hasActiveProduct(currentUser.getId(), productType)) {
	        log.info("Error: cannot purchase product " + productType + " the user already has it");
	        result = "purchaseError";
	    }
	    else {
	        log.info("Purchase product " + productType);
	        result = "redirect:/user/purchase/" + currentUser.getId().toString() + "/" + productType;
	    }

        return result;
    }
	
	@PreAuthorize("hasAuthority('USER')")
    @RequestMapping(value="/user/purchase/{userId}/{productType}", method=RequestMethod.GET)
    public String decideToPayForProduct(@PathVariable Long userId, @PathVariable String productType, Model model) {   
        log.info("Decide to pay " + productType);
        
        model.addAttribute("userId", userId);
        model.addAttribute("productType", productType);

        return "purchase";
    }
	
	@RequestMapping(value="/user/stripe/{userId}/{productType}", method=RequestMethod.POST)
	public String payUsingStripe(@PathVariable Long userId, @PathVariable String productType, @RequestParam Map<String, String> request, Model model) {
		String result = "paymentError";

		log.info("Paying for " + productType);

		model.addAttribute("productType", productType);

		// TODO: redirecting to secure payment gateway.
		// TODO: check for the result from gateway. If success, create a new product and show the corresponding page
		boolean paymentResult = false;
		
		// Get the credit card details submitted by the form
		String token = request.get("stripeToken");
		if (token != null) {
			paymentResult = processPayment(request, token);
		}

		// TODO: add results of payment to the model
		//model.addAttribute("cur", "eur"); //currency etc.

		if (paymentResult) {
			log.info("Creating new product " + productType + " after successful payment of user " + userId.toString());

			// TODO: Think how to handle expiration date. Now the default value is used.
			if (userService.createPurchaseProduct(userId, productType)) {
				// TODO: redirect to paymentResult page but without an ability to go back to the payment page
				result = "paymentResult";
			}
			else {
				log.info("There was an error while creating and saving the product " + productType);
			}
		}

		return result;
	}
	
	// TODO: implement this method (add stripe libraries)
	private boolean processPayment(Map<String, String> request, String token) {
		// Set your secret key: remember to change this to your live secret key
		// in production
		// See your keys here https://dashboard.stripe.com/account/apikeys
//		Stripe.apiKey = STRIPE_API_KEY;
//
//		// Create the charge on Stripe's servers - this will charge the user's
//		// card
//		try {
//			Map<String, Object> chargeParams = new HashMap<String, Object>();
//			chargeParams.put("amount", 1000); // amount in cents, again
//			chargeParams.put("currency", "eur");
//			chargeParams.put("source", token);
//			chargeParams.put("description", "Example charge");
//
//			Charge charge = Charge.create(chargeParams);
//			LOG.info("Payment charged to the following account: " + request);
//			LOG.debug("Charge: " + charge);
//		} catch (CardException | AuthenticationException | InvalidRequestException | APIConnectionException
//				| APIException e) {
//			// The card has been declined
//			LOG.error("Payment declined for account: " + request);
//		}
		
		return true;
	}
	
	@RequestMapping(value="/user/{userId}/product/{productId}", method=RequestMethod.GET)
    public String showProduct(@PathVariable Long userId, @PathVariable Long productId, Model model) {
	    // Disable new calculation link if the product has expired
	    final Boolean hasExpired = userService.hasProductExpired(userId, productId);
	    log.info("Product " + productId.toString() + " expired " + hasExpired.toString());
	    model.addAttribute("hasExpired", hasExpired);

        return userService.getProductPage(userId, productId, model);
    }

    // TODO (for all methods): do not expose the user id (product id) in the URL, just use it as a parameter, but do not show it in the URL
    @RequestMapping(value="/user/{userId}/product/{productId}/{calcId}", method=RequestMethod.GET)
    public String showCalculation(@PathVariable Long userId, @PathVariable Long productId, @PathVariable Long calcId, Model model) {

        return userService.getCalculationPage(userId, productId, calcId, model);
    }

    @RequestMapping(value="/user/{userId}/product/{productId}/{calcId}/{fileName}", method=RequestMethod.GET)
    @ResponseBody //TODO: think whether this annotation is needed
    public FileSystemResource downloadFile(@PathVariable Long userId, @PathVariable Long productId, @PathVariable Long calcId, @PathVariable String fileName) {

        return userService.getCalculationFile(userId, productId, calcId, fileName);
    }
    
    // NOTE: as I do not know how to handle polymorphism in Thymeleaf,
    // I decided to create separate input/calculate methods for each InputData implementation

    @RequestMapping(value="/user/{userId}/product/{productId}/parabola", method=RequestMethod.GET)
    public String showInput(@PathVariable Long userId, @PathVariable Long productId, Model model, ParabolaInputData inputData) {

        return showInputPage(userId, productId, model, inputData);
    }

    @RequestMapping(value="/user/{userId}/product/{productId}/hyperbola", method=RequestMethod.GET)
    public String showInput(@PathVariable Long userId, @PathVariable Long productId, Model model, HyperbolaInputData inputData) {

        return showInputPage(userId, productId, model, inputData);
    }
    
    @RequestMapping(value="/user/{userId}/product/{productId}/parabola", method=RequestMethod.POST)
    public String doCalculation(@PathVariable Long userId, @PathVariable Long productId, Model model, @Valid ParabolaInputData inputData, BindingResult bindingResult) {
        final boolean hasErrors = bindingResult.hasErrors();
        // TODO: returning the input web page should be done for concrete InputData types to enable automatic error checks
        
        return calculate(userId, productId, model, inputData, hasErrors);
    }
    
    @RequestMapping(value="/user/{userId}/product/{productId}/hyperbola", method=RequestMethod.POST)
    public String doCalculation(@PathVariable Long userId, @PathVariable Long productId, Model model, @Valid HyperbolaInputData inputData, BindingResult bindingResult) {
    	final boolean hasErrors = bindingResult.hasErrors();
    	// TODO: returning the input web page should be done for concrete InputData types to enable automatic error checks
    	
    	return calculate(userId, productId, model, inputData, hasErrors);
    }
    
    // TODO: figure out why no error messages are displayed for wrong input
    public String calculate(Long userId, Long productId, Model model, InputData inputData, boolean hasErrors) {
        String webPage = null;

        inputData.printData();
        
        if (!hasErrors) {
            webPage = userService.performAndSaveCalculation(userId, productId, inputData);
        }
        else {
            model.addAttribute("inputData", inputData); // TODO: see also model.addObject()
            webPage = userService.getInputPage(userId, productId, model);
        }

        return webPage;
    }
    
    private String showInputPage(Long userId, Long productId, Model model, InputData inputData) {
        String result = "redirect:/user/" + userId.toString() + "/product/" + productId.toString();
        
        if(!userService.hasProductExpired(userId, productId)) {
            model.addAttribute("inputData", inputData);
            result = userService.getInputPage(userId, productId, model);
        }

        return result;
    }

}

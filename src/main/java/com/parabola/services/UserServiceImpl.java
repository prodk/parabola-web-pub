package com.parabola.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.parabola.data.Calculation;
import com.parabola.data.InputData;
import com.parabola.data.Product;
import com.parabola.data.ProductStatus;
import com.parabola.data.RegistrationInfo;
import com.parabola.data.Role;
import com.parabola.data.User;
import com.parabola.repositories.UserRepository;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;
    
    private Logger log = Logger.getLogger(UserServiceImpl.class);

    @Autowired
    public void setUserRepository(UserRepository repo) {
        this.userRepository = repo;
    }

    @Override
    public Iterable<User> listAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public Iterable<Product> listUserProducts(Long userId) {
        final User user = userRepository.findOne(userId);
        List<Product> products = user.getActiveProducts();

        return products;
    }

    @Override
    public String getProductPage(Long userId, Long productId, Model model) {
        String webPage = "redirect:/";

        final User user = userRepository.findOne(userId);
        if (null != user) {
            final Product product = user.findProduct(productId);

            if (null != product) {
                model.addAttribute("p", product);
                model.addAttribute("history", product.getCalculations());
                webPage = product.getProductWebPage();
            }
        }

        return webPage;
    }

    @Override
    public String getInputPage(Long userId, Long productId, Model model) {
        String webPage = "redirect:/";

        final User user = userRepository.findOne(userId);
        if (null != user) {
            final Product product = user.findProduct(productId);

            if (null != product && !product.hasExpired()) {
                model.addAttribute("p", product);
                webPage = product.getInputWebPage();
            }
        }

        return webPage;
    }

    @Override
    public String getCalculationPage(Long userId, Long productId, Long calcId, Model model) {
        String webPage = "redirect:/";

        final User user = userRepository.findOne(userId);
        if (null != user) {
            final Product product = user.findProduct(productId);

            if (null != product) {
                model.addAttribute("p", product);
                model.addAttribute("calculation", product.getCalculation(calcId));
                webPage = product.getCalculationWebPage();
            }
        }

        return webPage;
    }

    @Override
    public FileSystemResource getCalculationFile(Long userId, Long productId, Long calcId, String fileName) {
        FileSystemResource result = null;
        final User user = userRepository.findOne(userId);
        if (null != user) {
            final Product product = user.findProduct(productId);

            if (null != product) {
                result = product.getCalculationFile(calcId, fileName);
            }
        }

        return result;
    }

    @Override
    public String performAndSaveCalculation(Long userId, Long productId, InputData inputData) {
        String webPage = "redirect:/";

        User user = userRepository.findOne(userId);
        if (null != user) {
            Product product = user.findProduct(productId);

            // TODO: make some notification that the product has expired
            if (null != product && !product.hasExpired()) {
                Calculation lastCalc = createAndSaveCalculation(user, product, inputData);

                if ((null != lastCalc) && lastCalc.doCalculation()) {
                    userRepository.save(user);                
                    webPage = "redirect:/user/" + user.getId() + "/product/" + product.getId().toString() + "/" + lastCalc.getId().toString();
                }
                else {
                    // TODO: return some error web page
                }                
            }
        }		

        return webPage;
    }

    private Calculation createAndSaveCalculation(User user, Product product, InputData inputData) {
        // IMPORTANT:
        // Getting the list of calculations and doing at least one operation with them (in our case getting the size)
        // is necessary to avoid a problem of creating 2 Calculation rows in the table when save is called
        List<Calculation> calcs = product.getCalculations();
        calcs.size();

        Calculation calc = new Calculation(inputData);
        calc.setProduct(product);
        product.addCalculation(calc);
        inputData.setCalculation(calc);
        // Important: add the newly created calculation to the repository.
        // This will automatically add the fields of the new calculation to the table and generates its id.
        userRepository.save(user);

        return product.getLastCalculation();
    }

	@Override
	public Optional<User> getUser(Long id) {
		return Optional.ofNullable(userRepository.findOne(id));
	}

	@Override
	public User getUserByEmail(String email) {
        User user = userRepository.findOneByEmail(email).get();
        return user;
    }

	@Override
	public boolean createTrialProduct(Long userId, String productType) {
	    boolean result = false;
	    // We need to retrieve the user from the repo to avoid duplicates when saving
	    User user = userRepository.findOne(userId);
	    
	    if (null != user) {
	        Product product = new Product(productType);
	        product.setPurchaseDate(new Date());
	        product.setUser(user);
	        user.addProduct(product);

	        userRepository.save(user);
	        
	        result = true; // TODO: check for the save(user) result
	    }
	    else {
	        log.info("createTrialProduct(): failed to find a user with id " + userId.toString());
	    }
	    
	    return result;
	}

    @Override
    public boolean createPurchaseProduct(Long userId, String productType) {
        boolean result = false;
     // We need to retrieve the user from the repo to avoid duplicates when saving
        User user = userRepository.findOne(userId);
        
        if (null != user) {
            Product product = new Product(productType);
            product.setPurchaseDate(new Date());
            product.setStatus(ProductStatus.purchased.toString());
            product.setUser(user);
            user.addProduct(product);

            userRepository.save(user);
            
            result = true; // TODO: check for the save(user) result
        }
        else {
            log.info("createTrialProduct(): failed to find a user with id " + userId.toString());
        }
        
        return result;
    }

    @Override
    public Iterable<Product> listUserExpiredProducts(Long userId) {
        final User user = userRepository.findOne(userId);
        List<Product> expiredProducts = user.getExpiredProducts();

        return expiredProducts;
    }

    @Override
    public boolean hasProduct(Long userId, String productType) {
        boolean result = false;
        
        final User user = userRepository.findOne(userId);
        if (null != user) {
            result = user.hasProduct(productType);
        }
        
        return result;
    }

    @Override
    public boolean hasProductExpired(Long userId, Long productId) {
        boolean result = false;

        final User user = userRepository.findOne(userId);
        if (null != user) {
            result = user.hasProductExpired(productId);
        }

        return result;
    }

    @Override
    public boolean hasActiveProduct(Long userId, String productType) {
        boolean result = false;

        final User user = userRepository.findOne(userId);
        if (null != user) {
            result = user.hasActiveProduct(productType);
        }

        return result;
    }
    
    @Override
    public boolean registerNewUserAccount(RegistrationInfo regInfo) {
        boolean result = false;
        
        // TODO: probably use EmailExistsException
        if (!emailExists(regInfo.getEmail())) {
            User user = new User();
            user.setFirstName(regInfo.getFirstName());
            user.setLastName(regInfo.getLastName());
            user.setPassword(regInfo.getPassword());
            user.setEmail(regInfo.getEmail());
            user.setRole(Role.USER);
            
            return (null != userRepository.save(user));
        }
        
        return result;
    }
    
    // TODO: probably move this check to the custom validator
    private boolean emailExists(String email) {     
        boolean result = false;
        User user = null;
        try {
        	user = getUserByEmail(email);
        	result = null != user;
        }
        catch (java.util.NoSuchElementException e) {
        	log.info("E-mail" + email + " was not found");
        }
        
        return result;
    }

}

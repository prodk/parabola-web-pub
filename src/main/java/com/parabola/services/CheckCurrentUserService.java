package com.parabola.services;

import com.parabola.data.CurrentUser;

public interface CheckCurrentUserService {
	boolean canAccessUser(CurrentUser currentUser, Long userId);
}

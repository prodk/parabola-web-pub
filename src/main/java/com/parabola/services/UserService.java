package com.parabola.services;

import java.util.Optional;

import org.springframework.core.io.FileSystemResource;
import org.springframework.ui.Model;

import com.parabola.data.InputData;
import com.parabola.data.Product;
import com.parabola.data.RegistrationInfo;
import com.parabola.data.User;

public interface UserService {
    Iterable<User> listAllUsers();
    Iterable<Product> listUserProducts(Long userId);

    String getProductPage(Long userId, Long productId, Model model);
    String getInputPage(Long userId, Long productId, Model model);

    String getCalculationPage(Long userId, Long productId, Long calcId, Model model);

    FileSystemResource getCalculationFile(Long userId, Long productId, Long calcId, String fileName);

    String performAndSaveCalculation(Long userId, Long productId, InputData inputData); // returns the web page of the corresponding calculation
    
    // TODO: check for duplicate methods, and probably use the methods below inside other methods
    Optional<User> getUser(Long id);
//    Optional<User> getUserByEmail(String email);
    User getUserByEmail(String email);
    
    boolean createTrialProduct(Long userId, String productType);
    
    boolean createPurchaseProduct(Long userId, String productType);
    
    Iterable<Product> listUserExpiredProducts(Long userId);
    
    boolean hasProduct(Long userId, String productType);
    
    boolean hasProductExpired(Long userId, Long productId);
    
    boolean hasActiveProduct(Long userId, String productType);
    
    boolean registerNewUserAccount(RegistrationInfo regInfo);
}

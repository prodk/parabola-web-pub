package com.parabola.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.parabola.data.User;

public interface UserRepository extends CrudRepository<User, Long>{
	Optional<User> findOneByEmail(String email);
}

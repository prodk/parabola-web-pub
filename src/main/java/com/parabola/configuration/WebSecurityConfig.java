package com.parabola.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.parabola.security.LoginSuccessHandler;

//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    
    private UserDetailsService userDetailsService;
    
    private LoginSuccessHandler successHandler;
    
    @Autowired
    public void setUserDetailsService(UserDetailsService service) {
        this.userDetailsService = service;
    }
    
    @Autowired
    public void setLoginSuccessHandler(LoginSuccessHandler handler) {
        this.successHandler = handler;
    }
    
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
                .antMatchers("/users/**").hasAuthority("ADMIN")
                .antMatchers("/register").permitAll()
                .antMatchers("/webjars/bootstrap/**").permitAll()
                .anyRequest().authenticated()
                .and()
            .formLogin()
                .loginPage("/login")
                .successHandler(successHandler)
                .usernameParameter("email")
                .permitAll()
                .and()
            .logout()
                .permitAll();
        
        // TODO: remove this for production version. Currently this enables the H2 console
        http.authorizeRequests().antMatchers("/console/**").permitAll();
        http.csrf().disable();
        http.headers().frameOptions().disable();
    }

//    @Autowired
//    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//        auth
//            .inMemoryAuthentication()
//                .withUser("user").password("password").roles("USER");
//    }
    
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(userDetailsService)
                // TODO: use encryption after it has been added to the User;
                .passwordEncoder(new BCryptPasswordEncoder());
    }

}

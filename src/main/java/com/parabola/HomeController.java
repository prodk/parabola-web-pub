package com.parabola;

import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.parabola.data.CurrentUser;
import com.parabola.data.Role;

@Controller
public class HomeController {
    
    private Logger log = Logger.getLogger(HomeController.class);
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    public String showHomePage(Authentication auth) {
        String result = null;
        
        CurrentUser currentUser = (CurrentUser) auth.getPrincipal();
        log.info("home ctrl /, role: " + currentUser.getRole()); 
        
        if (currentUser.getRole() == Role.ADMIN) {
            result = "redirect:/users";
        }
        else {
            result = "redirect:/user/" + currentUser.getId().toString();
        }

        return result;
    }
}
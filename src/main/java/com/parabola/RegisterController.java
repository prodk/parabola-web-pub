package com.parabola;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.parabola.data.RegistrationInfo;
import com.parabola.services.UserService;

@Controller
public class RegisterController {
    
    private UserService userService;
    
    @Autowired
    public void setUserService(UserService service) {
        this.userService = service;
    }
    
    private final static Logger LOG = Logger.getLogger(RegisterController.class);
    
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String getRegisterPage(Model model) {
        model.addAttribute("registrationInfo", new RegistrationInfo());
        
        LOG.info("Register");
        
        return "register";
    }
    
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String handleRegistration(Model model, @Valid  RegistrationInfo regInfo, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        String result = "register";
        
        if (bindingResult.hasErrors()) {
            LOG.info("handleRegistration(): there were input errors");
            model.addAttribute("registrationInfo", regInfo);
            
            return result;
        }
        
        LOG.info("handleRegistration(): user registration started");

        boolean registered = userService.registerNewUserAccount(regInfo);

        if (!registered) {
            bindingResult.rejectValue("email", "message.regError");
            model.addAttribute("registrationInfo", regInfo);
        }
        else
        {
            redirectAttributes.addFlashAttribute("alertSuccess", "Account successfully created! Please login.");
            result = "registrationResult";
        }
        
        return result;
    }

}

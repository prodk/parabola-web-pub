package com.parabola.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.apache.log4j.Logger;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class InputData {
    //@Transient
    //private final static Logger LOG = Logger.getLogger(InputData.class);
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @OneToOne
    private Calculation calc;
    
    private String binaryName = "parabola.exe";
    
    public abstract void createInputFile(Long calcId, String workingDir, String fileName);

    public abstract void printData();
    
    public boolean calculate(String fullPathInput, String fullPathOutput) throws IOException
    {
        boolean result = false;
        
        // TODO: reconsider paths to the binary/output/input files
        final String pathToBinary = System.getProperty("user.dir")+"/" + binaryName;

        final File binaryFile = new File(pathToBinary);
        if(binaryFile.exists() && !binaryFile.isDirectory()) { 
            System.out.println("Starting the parabola app: " + pathToBinary);


            Process process = new ProcessBuilder(pathToBinary, fullPathInput, fullPathOutput).start();

            Reader inStreamReader = new InputStreamReader(process.getInputStream());
            BufferedReader in = new BufferedReader(inStreamReader);

            String line;
            while ((line = in.readLine()) != null)
            {
                System.out.println(line);
            }
            in.close();

            System.out.println("Closed");
            
            int exitCode = -1;
            try {
                exitCode = process.waitFor();
                System.out.println("Calculations completed with error code " + exitCode);
            } catch (InterruptedException e) {
                System.out.println("InputData: an exception has occurred while waiting for the process to complete");
                e.printStackTrace();
            }           

            result = (exitCode == 0);
        }
        else {
            System.out.println("No binary found at this location: " + pathToBinary);
        }

        return result;
    }

    public Calculation getCalculation() {
        return calc;
    }
    
    public void setCalculation(Calculation c) {
        this.calc = c;
    }
}

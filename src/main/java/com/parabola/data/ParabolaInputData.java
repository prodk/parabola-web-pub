package com.parabola.data;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Transient;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;

@Entity
public class ParabolaInputData extends InputData {
    @Transient
    private Logger log = Logger.getLogger(ParabolaInputData.class);

    @DecimalMin("-5.0")
    @DecimalMax("5.0")
    private BigDecimal minX;

    @DecimalMin("-5.0")
    @DecimalMax("5.0")
    private BigDecimal maxX;

    @DecimalMin("0.0")
    @DecimalMax("3.0")
    private BigDecimal factor;

    @NotNull
    @Min(2)
    @Max(1000)
    private Integer numOfPoints;

    public BigDecimal getMinX() {
        return minX;
    }

    public void setMinX(BigDecimal minX) {
        this.minX = minX;
    }

    public BigDecimal getMaxX() {
        return maxX;
    }

    public void setMaxX(BigDecimal maxX) {
        this.maxX = maxX;
    }

    public BigDecimal getFactor() {
        return factor;
    }

    public void setFactor(BigDecimal factor) {
        this.factor = factor;
    }

    public Integer getNumOfPoints() {
        return numOfPoints;
    }

    public void setNumOfPoints(Integer numOfPoints) {
        this.numOfPoints = numOfPoints;
    }

    @Override
    public String toString() {
        return "ParabolaInputData [minX=" + minX + ", maxX=" + maxX + ", factor=" + factor + ", numOfPoints=" + numOfPoints
                + "]";
    }

    @Override
    public void createInputFile(Long calcId, String workingDir, String fileName) {
        final String fullPathInput = workingDir + "/"  + fileName;

        System.out.println("Writing it to the file: " + fullPathInput);
        
        if (minX.compareTo(maxX) == 1) {
            log.info("Warning: minx > maxX");
        }

        PrintWriter writer;
        try {
            writer = new PrintWriter(fullPathInput, "UTF-8");
            writer.println(minX);
            writer.println(maxX);
            writer.println(factor);
            writer.println(numOfPoints);
            writer.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }        
    }

    @Override
    public void printData() {
        System.out.println("* The following data received:");
        System.out.println("* minX = " + minX);
        System.out.println("* maxX = " + maxX);
        System.out.println("* factor = " + factor);
        System.out.println("* numOfPoints = " + numOfPoints);        
    }
}

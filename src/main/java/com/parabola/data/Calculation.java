package com.parabola.data;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

import javax.persistence.*;

import org.springframework.core.io.FileSystemResource;

@Entity
public class Calculation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private Product product;

    private Date creationTime;
    private Date startTime;
    private Date finishTime;
    private String outputFile;
    private String inputFile;
    
    @OneToOne(mappedBy = "calc", cascade = CascadeType.ALL)
    private InputData inputData;

    public enum Status {CREATED, SAVED, FINISHED, PENDING}; // TODO: probably remove some of these values if they are unused

    private Status status;

    private String fullPathInput;
    private String fullPathOutput;

    /// TODO: reconsider this constructor, use the id-based in/out files
    public Calculation() {
        this.creationTime = new Date(0L);
        this.startTime = new Date(1L);
        this.finishTime = new Date(2L);
        this.status = Status.CREATED;
        this.outputFile = "out.csv";
        this.inputFile = "in.csv";
    }

    public Calculation(InputData inputData) {
        this.startTime = new Date();
        this.finishTime = new Date();
        this.creationTime = new Date();
        this.status = Status.CREATED;

        this.inputData = inputData;
    }

    public Long getId() {
        return id;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date time) {
        this.startTime = time;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date time) {
        this.finishTime = time;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getOutputFile() {
        return outputFile;
    }

    public void setOutputFile(String outputFile) {
        this.outputFile = outputFile;
    }

    public String getInputFile() {
        return inputFile;
    }

    public void setInputFile(String file) {
        this.inputFile = file;
    }

    public InputData getInputData() {
        return inputData;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public void setFullPathInput(String path) {
        this.fullPathInput = path;
    }

    public String getFullPathInput() {
        return fullPathInput;
    }

    public void setFullPathOutput(String path) {
        this.fullPathOutput = path;
    }

    public String getFullPathOutput() {
        return fullPathOutput;
    }

    public FileSystemResource getFile(String fileName) {
        FileSystemResource result = null;
        final String fullFileName = fileName + ".csv";
        if (fullFileName.equals(outputFile)) {
            result = new FileSystemResource(new File(fullPathOutput));
        }
        else if (fullFileName.equals(inputFile)) {
            result = new FileSystemResource(new File(fullPathInput));
        }

        return result;
    }
    
    public boolean doCalculation() {
        boolean result = false;

        // TODO: check null ptr exceptions everywhere where new is used
        
        this.outputFile = "out_" + product.getType() + "_" + id.toString() +".csv"; 
        this.inputFile = "in_" + product.getType() + "_" + id.toString() + ".csv";

        // TODO: reconsider paths to the binary/output/input files (when deploying to a real server)
        String workingDir = createWorkingDirectory();

        inputData.createInputFile(id, workingDir, this.inputFile);

        final String fullPathInput = workingDir + "/"  + this.inputFile;
        final String fullPathOutput = workingDir + "/" + this.outputFile;
        setFullPathInput(fullPathInput);
        setFullPathOutput(fullPathOutput);

        try {
            // TODO: remove this, it is just for debugging the time
            delay(2000L);

            setStartTime(new Date());
            setStatus(Status.PENDING); // TODO: to see this change save the user to user repository
            if (inputData.calculate(fullPathInput, fullPathOutput)) {

                result = true;
                
                // TODO: remove this, it is just for debugging the time
                delay(2000L);

                setFinishTime(new Date());
                setStatus(Status.FINISHED);
            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return result;
    }
    
    private String createWorkingDirectory() {
        // TODO: make paths configurable somehow
        //String workingDir = System.getProperty("user.dir") + "/data";
        String workingDir = System.getProperty("user.home") + "/data";
        checkDirectoryExists(workingDir);

        workingDir = workingDir + "/" + "user_" + product.getUser().getId().toString();
        checkDirectoryExists(workingDir);

        workingDir = workingDir + "/" + product.getType() + "_" + product.getId().toString();
        checkDirectoryExists(workingDir);
        
        workingDir = workingDir + "/calc_" + getId().toString();
        checkDirectoryExists(workingDir);

        return workingDir;
    }

    Path checkDirectoryExists(String directoryPath) {
        Path path = Paths.get(directoryPath);
        if (!Files.exists(path)) {
            try {
                Files.createDirectory(path);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return path;
    }
    
    // Just for debugging
    private void delay(Long duration) {
        try {
            Thread.sleep(duration);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }
}

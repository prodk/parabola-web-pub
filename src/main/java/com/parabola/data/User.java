package com.parabola.data;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Entity
@Table(name="\"User\"")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "user", cascade = CascadeType.ALL)
	private List<Product> products;
	
	@Column(name = "email", nullable = false, unique = true)
	private String email;
	
	@Column(name = "password", nullable = false)
	private String password;
	
	// TODO: uncomment this later to disable null roles
	//@Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role;
	
	private String firstName;
	private String lastName;
	
	public User() {
		this.products = new ArrayList<Product>();
		this.role = Role.USER;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getId() {
		return id;
	}
	
	public void addProduct(Product p) {
		products.add(p);
	}
	
	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
	public List<Product> getProducts() {
		return products;
	}
	
	public Product findProduct(Long productId) {
		Product result = null;
		
		for (Product p : products) {
			if (productId == p.getId()) {
				result = p;
				break;
			}
		}
		
		return result;
	}
	
	public void setEmail(String login) {
		this.email = login;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setPassword(String pwd) {
		
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		this.password = encoder.encode(pwd);
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setRole(Role r) {
		this.role = r;
	}
	
	public Role getRole() {
		return role;
	}
	
	public List<Product>  getExpiredProducts() {
	    List<Product> expiredProducts = new ArrayList<Product>();
        
        for (Product p : products) {
            if (p.hasExpired()) {
                expiredProducts.add(p);
            }
        }
        
        return expiredProducts;
    }
	
	public List<Product>  getActiveProducts() {
        List<Product> activeProducts = new ArrayList<Product>();
        
        for (Product p : products) {
            if (!p.hasExpired()) {
                activeProducts.add(p);
            }
        }
        
        return activeProducts;
    }
	
	public boolean hasActiveProduct(String productType) {
	    boolean result = false;
	    
	    List<Product> activeProducts = getActiveProducts();
	    for (Product p : activeProducts) {
            if (p.getType().equals(productType)) {
                result = true;
                break;
            }
        }
	    
	    return result;
	}
	
	public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean hasProduct(String productType) {
        boolean result = false;
        
        for (Product p : products) {
            if (p.getType().equals(productType)) {
                result = true;
                break;
            }
        }
        
        return result;
    }
	
	public boolean hasProductExpired(Long productId) {
	    // Assume the worst case
	    boolean result = true;
	    
	    // If the product is active, then it is not expired
	    List<Product> activeProducts = getActiveProducts();
	    for (Product p : activeProducts) {
            if (p.getId() == productId) {
                result = false;
                break;
            }
        }
	    
	    return result;
	}
}

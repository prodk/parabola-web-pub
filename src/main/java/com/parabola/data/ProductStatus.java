package com.parabola.data;

public enum ProductStatus {
    trial, purchased
}

package com.parabola.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.core.io.FileSystemResource;

@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String type;

    @ManyToOne
    private User user;

    @OneToMany(mappedBy="product", cascade = CascadeType.ALL) //TODO: see what mappedBy and cascade mean
    private List<Calculation> calculations;

    //TODO: map product type onto a nice product name, e.g. "PARABOLA" -> "Parabola"

    //TODO: probably use @Temporal
    private Date purchaseDate;
    private Date expirationDate;
    
    // TODO: probably make an enum
    private String status;
    
    // TODO: probably remove this variable
    private boolean hasExpired;
    
    private final String productPage = "product";
    private final String calculationPage = "calculation";
    
    private int maxNumOfTrialCalcs = 3;

    public Product() { // Obligatory: needed for JPA
        this.calculations = new ArrayList<Calculation>();
        this.status = ProductStatus.trial.toString();
        this.purchaseDate = new Date();
        
        // TODO: add expiration interval to the constructor
        this.expirationDate = this.purchaseDate;
        expirationDate.setMinutes(purchaseDate.getMinutes() + 8);
        
        setExpired(false);
    }

    public Product(String type) {
        super();
        this.type = type;
        this.calculations = new ArrayList<Calculation>();
        this.status = ProductStatus.trial.toString();

        this.purchaseDate = new Date();
        this.expirationDate = this.purchaseDate;
        // TODO: add expiration interval to the constructor
        expirationDate.setMinutes(purchaseDate.getMinutes() + 8);
        
        setExpired(false);
    }

    public Long getId() {
        return id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setUser(User u) {
        this.user = u;
    }

    public User getUser() {
        return user;
    }

    public void setPurchaseDate(Date date) {
        this.purchaseDate = date;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setExpirationDate(Date date) {
        this.expirationDate = date;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public List<Calculation> getCalculations() {
        return calculations;
    }

    public Calculation getLastCalculation() {
        Calculation result = null;
        final int numOfCalcs = calculations.size();
        if (numOfCalcs > 0) {
            result = calculations.get(numOfCalcs - 1);
        }
        return result;
    }

    public Calculation getCalculation(Long id) {
        Calculation result = null;

        for (Calculation c : calculations) {
            if (id == c.getId()) {
                result = c;
                break;
            }
        }

        return result;
    }

    public void addCalculation(Calculation c) {
        this.calculations.add(c);
    }

    public String getProductWebPage() {
        return productPage;
    }

    public String getCalculationWebPage() {
        return calculationPage;
    }

    public String getInputWebPage() {
        return type + "Input";
    }

    public FileSystemResource getCalculationFile(Long calcId, String fileName) {
        FileSystemResource result = null;

        for (Calculation c : calculations) {
            if (calcId == c.getId()) {
                result = c.getFile(fileName);
                break;
            }
        }

        return result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean hasExpired() {
        Date currentDate = new Date();
        
        hasExpired = currentDate.after(expirationDate);
        
        hasExpired = hasExpired || (status.equals(ProductStatus.trial.toString()) && (calculations.size() >= maxNumOfTrialCalcs));
        
        return hasExpired;
    }

    public void setExpired(boolean hasExpired) {
        this.hasExpired = hasExpired;
    }
}

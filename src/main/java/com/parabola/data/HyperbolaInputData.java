package com.parabola.data;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import javax.persistence.Entity;

@Entity
public class HyperbolaInputData extends InputData {

    @Override
    public void createInputFile(Long calcId, String workingDir, String fileName) {
        final String fullPathInput = workingDir + "/"  + fileName;

        System.out.println("Writing it to the file: " + fullPathInput);

        PrintWriter writer;
        try {
            writer = new PrintWriter(fullPathInput, "UTF-8");
            writer.println("Hyperbola input");
            writer.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }        
    }

    @Override
    public void printData() {
        System.out.println("* HyperbolaInput * printData()");        
    }
    
    @Override
    public boolean calculate(String fullPathInput, String fullPathOutput) throws IOException {
        System.out.println("* HyperbolaInput * calculate()");
        
        PrintWriter writer;
        try {
            writer = new PrintWriter(fullPathOutput, "UTF-8");
            writer.println("Hyperbola output");
            writer.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }   
        
        return true;        
    }
}

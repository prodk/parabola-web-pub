package com.parabola.data;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {   
    @Override
    public void initialize(PasswordMatches constraintAnnotation) {       
    }
    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context){   
        RegistrationInfo userInfo = (RegistrationInfo) obj;
        return userInfo.getPassword().equals(userInfo.getMatchingPassword());
    }  
}

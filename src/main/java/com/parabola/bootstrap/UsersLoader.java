package com.parabola.bootstrap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.parabola.data.Role;
import com.parabola.data.User;
import com.parabola.repositories.UserRepository;

@Component
public class UsersLoader implements ApplicationListener<ContextRefreshedEvent> {

    private UserRepository repository;

    private Logger log = Logger.getLogger(UsersLoader.class);

    @Autowired
    public void setUserRepository(UserRepository repo) {
        this.repository = repo;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent arg0) {
        //User user0 = new User();        
        //user0.setRole(Role.USER);
        //user0.setEmail("loin@login.com");
        //user0.setPassword("ha");
        //repository.save(user0);
        //log.info("Saved user" + user0.getId());	

        // TODO: do not save users on the 2nd run
//        User user1 = new User();        
//        user1.setRole(Role.ADMIN);
//        user1.setEmail("kolya@login.com");
//        user1.setPassword("sumy2016");
//
//        repository.save(user1);
//
//        log.info("Saved user" + user1.getId());		
    }

}

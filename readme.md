### **parabola-web-pub** ###

## A simple example of a web application to perform computations on the server side. ##

It shows how a company might allow their users to perform computations on a server using their (computational) software products.

The app has the functionality:

1) to register users with different rights (admin/non-admin);

2) depending on the user's rights manage the registered users;

3) have several 'products' which denote a particular computation that can be performed on the server;

4) each user can buy the number of computations that he wants to perform using a particular product;

5) each user can provide input parameters for every product and start/stop the computation;

6) the history of all the computations (their parameters, start/stop time, resulting output) is saved on the server for every user.

A very simple 'parabola.exe' is used as an example of a product. It calculates a parabola for the given number of points.

## Implemented using the Spring framework (Spring boot) and Thymeleaf. ##